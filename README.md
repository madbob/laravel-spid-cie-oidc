# Autenticazione SPID/CIE per Laravel

A dispetto del nome, questo pacchetto semplifica l'autenticazione SPID/CIE sia su protocollo SAML che su OIDC per le applicazioni Laravel.

Si tratta essenzialmente di una pesante revisione dei pacchetti ufficiali `spid-cie-php` e `spid-cie-oidc-php`, rimaneggiati per essere più semplici da installare, configurare, ed integrare nella propria applicazione.

Per supporto e consulenza, manda una mail a info@madbob.org

## Installazione

```
# Installa il pacchetto nel tuo progetto Laravel
composer require madbob/laravel-spid-cie-oidc

# Inizializza il file di configurazione in config/spid-cie-oidc.php
# È raccomandato revisionare e correggere i parametri di questo file di
# configurazione prima di eseguire i successivi comandi, in particolare la
# sezione "organization" e l'abilitazione dei protocolli SAML e/o OIDC
php artisan vendor:publish --provider="MadBob\SCOL\SCOLServiceProvider" --tag=config

# Genera i certificati
php artisan scol:make-certificates

# Per abilitare l'autenticazione SAML
php artisan scol:deploy-saml
```

Il comando `php artisan scol:deploy-saml` va eseguito ogni volta che si modifica qualcosa nel proprio file `config/spid-cie-oidc.php`, al fine di riportare i propri aggiornamenti anche nella configurazione statica di SimpleSAML. Il comando `php artisan scol:make-certificates` genera i certificati nei path configurati, e - nota bene! - sovrascrive eventuali file già esistenti.

Un passaggio importante del setup iniziale è la predisposizione delle proprie callback di autenticazione: è necessario predisporre una propria classe che implementi l'interfaccia `MadBob\SCOL\Interfaces\UserLogin` e specificarne il path nella configurazione (in `spid-cie-oidc.operations.login`). Nel file `src/Helpers/DefaultLogin.php` se ne trova un esempio con i relativi dettagli. Eventualmente si può fare altrettanto per gestire il logout, aggiungendo una propria classe che implementi l'interfaccia `MadBob\SCOL\Interfaces\UserLogout`.

Nel proprio pannello di login, si può usare il seguente codice per ottenere i diversi link che attivano la procedura di autenticazione sui diversi sistemi:

```
$buttons = new \MadBob\SCOL\Helpers\Buttons();

$cie_link = $buttons->buttonCie();
if ($cie_link) {
    echo '<a href="' . $cie_link . '">Entra con CIE</a>';
}

$spid_providers = $buttons->buttonSpid();
if ($spid_providers) {
    foreach($spid_providers as $provider) {
        echo '<a href="' . $provider->link . '">Entra con ' . $provider->name . '</a>';
    }
}
```

## Note

I files che si trovano nella cartella `deps/config` e `deps/lib` sono stati copiati interamente da [questo repository](https://github.com/rglauco/spid-cie-oidc-php), che a sua volta è un fork - riveduto e corretto - di [quest'altro repository](https://github.com/italia/spid-cie-oidc-php). Molti dei files qui presenti non sono utilizzati da questo modulo, ma si è preferito conservare l'intera cartella ed utilizzare direttamente quel codice senza modifiche per semplificare eventuali aggiornamenti futuri.

I files che si trovano nella cartella `deps/saml` sono stati copiati da [questo repository](https://github.com/italia/spid-cie-php) e, dove necessario, adattati a SimpleSAML versione 2 (l'implementazione originaria è basata su SimpleSAML 1.19.8, che a causa delle dipendenze non è compatibile con le versioni più aggiornate di Laravel). Di fatto sovrascrivono alcune classi interne di SimpleSAML "squattandone" il namespace, al fine di introdurre forzatamente le idiosincrasie proprie del protocollo SPID all'interno del protocollo SAML: se in fase di installazione con Composer vedi una sfilza di "Warning: Ambiguous class resolution", sappi che è tristemente "normale"...

Tutti i files generati da questo modulo (certificati, databases...) sono conservati nella cartella `storage/oidc`, che deve dunque essere scrivibile dal web server (come del resto tutta la cartella `storage`).

Poiché non di rado registry.spid.gov.it si scassa e non ritorna l'elenco degli IdP SPID, quando possibile l'elenco degli identity provider viene messo nella [cache di Laravel](https://laravel.com/docs/cache) e quando necessario viene da lì recuperato. Si tenga presente dunque che alcuni valori potrebbero non essere aggiornati all'ultimissima versione erogata dallo SPID Registry: all'occorrenza, si può usare il comando `php artisan cache:clear` per ripulire tutte le cache.

### SAML

Per l'abilitazione di SAML (attualmente: l'unica con cui è possibile autenticarsi con SPID) è necessario qualche accorgimento ulteriore.

Eseguendo il comando `php artisan scol:deploy-saml`, nella configurazione SimpleSAML viene iscritto come path di base il valore della configurazione `spid-cie-oidc.routes.saml.module`, il quale path va poi gestito nella configurazione del proprio web server per fungere da alias nei confronti della cartella pubblica di SimpleSAML stesso. Qui di seguito una configurazione di riferimento per nginx, in cui si assume che `spid-cie-oidc.routes.saml.module` sia valorizzato con il valore `spidcie`.

```
server {
        listen 443 ssl;
        listen [::]:443 ssl;
        server_name tuodominio.it;
        root /var/www/tuodominio/public/;

        # ... altri parametri della configurazione per Laravel...

        location /spidcie/ {
            alias /var/www/tuopath/vendor/simplesamlphp/simplesamlphp/public/;
            index index.php;
            location ~ ^(?<prefix>/spidcie)(?<phpfile>.+?\.php)(?<pathinfo>/.*)?$ {
                fastcgi_param SCRIPT_FILENAME $document_root$phpfile;
                fastcgi_param PATH_INFO $pathinfo if_not_empty;

                # Questo serve solo se si definisce un path esplicito per la
                # configurazione di SimpleSAML in spid-cie-oidc.saml.simplesaml.folder
                # Se spid-cie-oidc.saml.simplesaml.folder == false, questa riga
                # può essere omessa
                fastcgi_param SIMPLESAMLPHP_CONFIG_DIR /var/www/tuodominio/storage/oidc/simplesaml/;

                fastcgi_pass unix:/run/php/php8.2-fpm.sock;
                include fastcgi_params;
            }
        }
}
```

## Licenza

laravel-spid-cie-oidc è distribuito in licenza Apache 2.

Copyright holders:

- Roberto Guido @madbob <info@madbob.org>
- Glauco Rampogna @rglauco
- Michele D'Amico @damikael <michele.damico@linfaservice.it>
