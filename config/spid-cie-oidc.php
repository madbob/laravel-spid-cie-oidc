<?php

return [
    /*
        debug = true : abilita sia l'endpoint di pre-produzione CIE che
        Validator + Demo SPID (il parametro "validator" viene ignorato)
        debug = false && validator = true : abilita solo Validator + Demo SPID
    */
    'debug' => env('APP_DEBUG'),
    'validator' => false,

    /*
        Qui ci vanno le informazioni relative all'applicazione e
        all'organizzazione cui è intestata.
        Attenzione: i valori (in particolare: nome e email) devono corrispondere
        *esattamente* a quelli usati in fase di registrazione dell'ente cui
        appartiene l'istanza, soprattutto sul portale della federazione CIE/OIDC
    */
    'organization' => [
        'name' => 'Nome del Servizio',
        'locality' => 'Roma',

        // Codice Istat del comune in "locality"
        'istatLocality' => 'H501',

        // Sigla della provincia
        'district' => 'RM',

        'is_pa' => true,
        'code' => 'code',
        'identifier' => 'PA:it-code',

        'home' => env('APP_URL'),
        'logo' => env('APP_URL') . '/assets/images/logo.png',
        'policy_url' => env('APP_URL') . '/privacy-policy',

        'contacts' => [
            'email' => 'info@example.com',

            // Reminder: il numero di telefono *deve* iniziare con +39
            'phone' => '+390123456789',
        ],
    ],

    /*
        Qui ci vanno le informazioni relative al fornitore di servizio.
        Ovvero: tu che stai implementando questa libreria!
    */
    'technical' => [
        'name' => 'Nome del Fornitore',
        'email' => 'info@example.com',
    ],

    /*
        Per tramite di questo array è possibile abilitare o disabilitare ciascun
        metodo di autenticazione. I valori possibili per "cie" e "spid" sono
        - boolean: false = tipo di autenticazione disabilitato
        - string: "saml" = utilizza il protocollo SAML
        - string: "oidc" = utilizza il protocollo OIDC

        NOTA BENE: l'implementazione OIDC per SPID non esiste, dunque *non*
        usare il valore "oidc" per l'attributo "spid"
    */
    'protocols' => [
        'cie' => 'oidc',
        'spid' => 'saml',
    ],

    'saml' => [
        /*
            Eventuale entityID personalizzato per l'istanza SAML.
            È ragionevole lasciarlo a NULL, e lasciare che venga usato il valore
            di env('APP_URL'): viene definito qui per casi particolari di
            applicazioni che sono già state federate in SPID con un entityID
            diverso
        */
        'entityid' => null,

        'simplesaml' => [
            /*
                Se configurato con un path assoluto, salva la configurazione
                SimpleSAML nella cartella desiderata e da lì vi attinge i
                contenuti. Altrimenti, opera nelle cartelle di default (dentro
                vendor).
                Questo parametro viene definito per permettere di gestire
                molteplici service provider (e dunque: molteplici configurazioni
                SimpleSAML diverse) in una unica applicazione Laravel.
                All'occorrenza, è necessario anche specificare il parametro
                SIMPLESAMLPHP_CONFIG_DIR nell'environment del web server: cfr.
                file README.txt per ulteriori informazioni
            */
            'folder' => false,
        ],
    ],

    /*
        Qui ci vanno le informazioni relative all'istanza federata OIDC.
        Nota che alcuni parametri certamente devono essere cambiati passando
        dall'ambiente di preproduzione a quello di produzione!
    */
    'client' => [
        'id' => env('APP_URL'),
        'name' => env('APP_NAME'),

        'acr' => [
            'https://www.spid.gov.it/SpidL1',
            'https://www.spid.gov.it/SpidL2',
        ],

        /*
            Gli attributi che si intendono recuperare per l'utente autenticato.
            "Ovviamente" SAML e OIDC utilizzano identificativi diversi: si
            raccomanda di consultare la documentazione ufficiale
        */
        'user_attributes' => [
            'oidc' => [
                'given_name',
                'family_name',
                'https://attributes.eid.gov.it/fiscal_number',
            ],
            'saml' => [
                'name',
                'familyName',
                'fiscalNumber',
            ],
        ],

        'authority_hint' => 'http://trust-anchor.org:8000',

        'cie' => [
            'provider' => 'http://cie-provider.org:8002/oidc/op',
        ],

        'trust_marks' => [
            /*

            // Il formato atteso per gli elementi in questo array è il seguente:

            (object) [
                'trust_mark' => "",
                'iss' => "https://oidc.registry.servizicie.interno.gov.it",
                'id' => "https://oidc.registry.servizicie.interno.gov.it/openid_relying_party/public"
            ],
            */
        ],
    ],

    'views' => [
        /*
            In caso di errore, viene generata una risposta con questo template
            cui vengono passati i seguenti parametri
            - error_description: testo dell'errore

            Se il template non esiste, o questo parametro non viene valorizzato,
            viene usato il template di default di Laravel per la segnalazione
            degli errori
        */
        'errors' => 'your.own.error.template',
    ],

    /*
        Paths dei certificati.
        Il comando scol:make-certificates usa questi path per scrivere (o
        sovrascrivere!) i files
    */
    'certs' => [
        /* Chiavi OIDC */
        'private_core_sig' => storage_path("oidc/certs/rp.pem"),
        'public_core_sig' => storage_path("oidc/certs/rp.crt"),
        'private_core_enc' => storage_path("oidc/certs/rp-enc.pem"),
        'public_core_enc' => storage_path("oidc/certs/rp-enc.crt"),
        'private_fed_sig' => storage_path("oidc/certs/rp-fed.pem"),
        'public_fed_sig' => storage_path("oidc/certs/rp-fed.crt"),

        /* Chiavi SAML */
        'saml_spid_pem' => storage_path("oidc/certs/spid-sp.pem"),
        'saml_spid_crt' => storage_path("oidc/certs/spid-sp.crt"),
        'saml_cie_pem' => storage_path("oidc/certs/cie-sp.pem"),
        'saml_cie_crt' => storage_path("oidc/certs/cie-sp.crt"),
    ],

    'routes' => [
        'saml' => [
            /*
                "prefix" è il prefisso usato per le rotte interne
                "module" è dove viene linkato il frontend di SimpleSAML
            */
            'prefix' => 'saml',
            'module' => 'spidcie',
        ],

        'oidc' => [
            'prefix' => 'oidc',
        ],

        'middleware' => ['web'],
    ],

    'operations' => [
        'login' => \MadBob\SCOL\Helpers\DefaultLogin::class,
        'logout' => \MadBob\SCOL\Helpers\DefaultLogout::class,
    ],
];
