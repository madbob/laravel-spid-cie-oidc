<?php

/*
    Questo comando riscrive i files di configurazione di SimpleSAML attingendo
    principalmente dal file config/spid-cie-oidc.php
*/

namespace MadBob\SCOL\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class DeploySaml extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scol:deploy-saml';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Configura SimpleSAML';

    private function idpCie($metadata)
    {
        /* CIE Produzione */
        $metadata['https://idserver.servizicie.interno.gov.it/idp/profile/SAML2/POST/SSO'] = [
            'description' => [
                'en' => 'Identification Provider using Italy\'s electronic ID card (CIE)',
                'it' => 'Provider di autenticazione con CIE (Carta di Identità Elettronica)',
            ],
            'icon' => 'https://idserver.servizicie.interno.gov.it/idp/images/android1.jpg',
            'OrganizationName' => [
                'en' => 'Ministry of the Interior',
                'it' => 'Ministero dell\'Interno',
            ],
            'name' => [
                'en' => 'Italy\'s eID card (CIE)',
                'it' => 'CIE (Cartà di Identità Elettronica)',
            ],
            'OrganizationDisplayName' => [
                'en' => 'Ministry of the Interior',
                'it' => 'Ministero dell\'Interno',
            ],
            'OrganizationURL' => [
                'en' => 'https://www.interno.gov.it/it',
                'it' => 'https://www.interno.gov.it/en',
            ],
            'sign.authnrequest' => true,
            'sign.logout' => true,
            'SingleSignOnService' => [
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                    'Location' => 'https://idserver.servizicie.interno.gov.it/idp/profile/SAML2/Redirect/SSO',
                ],
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                    'Location' => 'https://idserver.servizicie.interno.gov.it/idp/profile/SAML2/POST/SSO',
                ],
                [
                    'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
                    'Location' => 'https://idserver.servizicie.interno.gov.it/idp/profile/Shibboleth/SSO',
                ],
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
                    'Location' => 'https://idserver.servizicie.interno.gov.it/idp/profile/SAML2/POST-SimpleSign/SSO',
                ],
            ],
            'SingleLogoutService' => [
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                    'Location' => 'https://idserver.servizicie.interno.gov.it/idp/profile/SAML2/Redirect/SLO',
                ],
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                    'Location' => 'https://idserver.servizicie.interno.gov.it/idp/profile/SAML2/POST/SLO',
                ],
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
                    'Location' => 'https://idserver.servizicie.interno.gov.it/idp/profile/SAML2/POST-SimpleSign/SLO',
                ],
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
                    'Location' => 'https://idserver.servizicie.interno.gov.it/idp/profile/SAML2/SOAP/SLO',
                ],
            ],
            'ArtifactResolutionService' => [
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:1.0:bindings:SOAP-binding',
                    'Location' => 'https://idserver.servizicie.interno.gov.it/idp/profile/SAML1/SOAP/ArtifactResolution',
                ],
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
                    'Location' => 'https://idserver.servizicie.interno.gov.it/idp/profile/SAML2/SOAP/ArtifactResolution',
                ],
            ],
            'NameIDPolicy' => [
                'Format' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
                'AllowCreate' => true,
            ],

            'certData' => 'MIIDdTCCAl2gAwIBAgIUU79XEfveueyClDtLkqUlSPZ2o8owDQYJKoZIhvcNAQELBQAwLTErMCkGA1UEAwwiaWRzZXJ2ZXIuc2Vydml6aWNpZS5pbnRlcm5vLmdvdi5pdDAeFw0xODEwMTkwODM1MDVaFw0zODEwMTkwODM1MDVaMC0xKzApBgNVBAMMImlkc2VydmVyLnNlcnZpemljaWUuaW50ZXJuby5nb3YuaXQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHraj3iOTCIILTlOzicSEuFt03kKvQDqGWRd5o7s1W7SP2EtcTmg3xron/sbrLEL/eMUQV/Biz6J4pEGoFpMZQHGxOVypmO7Nc8pkFot7yUTApr6Ikuy4cUtbx0g5fkQLNb3upIg0Vg1jSnRXEvUCygr/9EeKCUOi/2ptmOVSLad+dT7TiRsZTwY3FvRWcleDfyYwcIMgz5dLSNLMZqwzQZK1DzvWeD6aGtBKCYPRftacHoESD+6bhukHZ6w95foRMJLOaBpkp+XfugFQioYvrM0AB1YQZ5DCQRhhc8jejwdY+bOB3eZ1lJY7Oannfu6XPW2fcknelyPt7PGf22rNfAgMBAAGjgYwwgYkwHQYDVR0OBBYEFK3Ah+Do3/zB9XjZ66i4biDpUEbAMGgGA1UdEQRhMF+CImlkc2VydmVyLnNlcnZpemljaWUuaW50ZXJuby5nb3YuaXSGOWh0dHBzOi8vaWRzZXJ2ZXIuc2Vydml6aWNpZS5pbnRlcm5vLmdvdi5pdC9pZHAvc2hpYmJvbGV0aDANBgkqhkiG9w0BAQsFAAOCAQEAVtpn/s+lYVf42pAtdgJnGTaSIy8KxHeZobKNYNFEY/XTaZEt9QeV5efUMBVVhxKTTHN0046DR96WFYXs4PJ9Fpyq6Hmy3k/oUdmHJ1c2bwWF/nZ82CwOO081Yg0GBcfPEmKLUGOBK8T55ncW+RSZadvWTyhTtQhLUtLKcWyzKB5aS3kEE5LSzR8sw3owln9P41Mz+QtL3WeNESRHW0qoQkFotYXXW6Rvh69+GyzJLxvq2qd7D1qoJgOMrarshBKKPk+ABaLYoEf/cru4e0RDIp2mD0jkGOGDkn9XUl+3ddALq/osTki6CEawkhiZEo6ABEAjEWNkH9W3/ZzvJnWo6Q==',
        ];

        /* CIE Preproduzione */
        $metadata['https://preproduzione.idserver.servizicie.interno.gov.it/idp/profile/SAML2/POST/SSO'] = [
            'description' => [
                'en' => 'Identification Provider using Italy\'s electronic ID card (CIE) (test environment)',
                'it' => 'Provider di autenticazione con CIE (Carta di Identità Elettronica) (ambiente di test)',
            ],
            'icon' => 'https://idserver.servizicie.interno.gov.it/idp/images/android1.jpg',
            'OrganizationName' => [
                'en' => 'Ministry of the Iterior',
                'it' => 'Ministero dell\'Interno',
            ],
            'name' => [
                'en' => 'Italy\'s eID card (CIE) (test)',
                'it' => 'CIE (Cartà di Identità Elettronica) (test)',
            ],
            'OrganizationDisplayName' => [
                'en' => 'Ministry of the Iterior',
                'it' => 'Ministero dell\'Interno',
            ],
            'OrganizationURL' => [
                'en' => 'https://www.interno.gov.it/it',
                'it' => 'https://www.interno.gov.it/en',
            ],
            'sign.authnrequest' => true,
            'sign.logout' => true,
            'SingleSignOnService' => [
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                    'Location' => 'https://preproduzione.idserver.servizicie.interno.gov.it/idp/profile/SAML2/Redirect/SSO',
                ],
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                    'Location' => 'https://preproduzione.idserver.servizicie.interno.gov.it/idp/profile/SAML2/POST/SSO',
                ],
                [
                    'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
                    'Location' => 'https://preproduzione.idserver.servizicie.interno.gov.it/idp/profile/Shibboleth/SSO',
                ],
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
                    'Location' => 'https://preproduzione.idserver.servizicie.interno.gov.it/idp/profile/SAML2/POST-SimpleSign/SSO',
                ],
            ],
            'SingleLogoutService' => [
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                    'Location' => 'https://preproduzione.idserver.servizicie.interno.gov.it/idp/profile/SAML2/Redirect/SLO',
                ],
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                    'Location' => 'https://preproduzione.idserver.servizicie.interno.gov.it/idp/profile/SAML2/POST/SLO',
                ],
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
                    'Location' => 'https://preproduzione.idserver.servizicie.interno.gov.it/idp/profile/SAML2/POST-SimpleSign/SLO',
                ],
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
                    'Location' => 'https://preproduzione.idserver.servizicie.interno.gov.it/idp/profile/SAML2/SOAP/SLO',
                ],
            ],
            'ArtifactResolutionService' => [
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:1.0:bindings:SOAP-binding',
                    'Location' => 'https://preproduzione.idserver.servizicie.interno.gov.it/idp/profile/SAML1/SOAP/ArtifactResolution',
                ],
                [
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
                    'Location' => 'https://preproduzione.idserver.servizicie.interno.gov.it/idp/profile/SAML2/SOAP/ArtifactResolution',
                ],
            ],
            'NameIDPolicy' => [
                'Format' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
                'AllowCreate' => true,
            ],

            'certData' => 'MIIDdTCCAl2gAwIBAgIUU79XEfveueyClDtLkqUlSPZ2o8owDQYJKoZIhvcNAQELBQAwLTErMCkGA1UEAwwiaWRzZXJ2ZXIuc2Vydml6aWNpZS5pbnRlcm5vLmdvdi5pdDAeFw0xODEwMTkwODM1MDVaFw0zODEwMTkwODM1MDVaMC0xKzApBgNVBAMMImlkc2VydmVyLnNlcnZpemljaWUuaW50ZXJuby5nb3YuaXQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHraj3iOTCIILTlOzicSEuFt03kKvQDqGWRd5o7s1W7SP2EtcTmg3xron/sbrLEL/eMUQV/Biz6J4pEGoFpMZQHGxOVypmO7Nc8pkFot7yUTApr6Ikuy4cUtbx0g5fkQLNb3upIg0Vg1jSnRXEvUCygr/9EeKCUOi/2ptmOVSLad+dT7TiRsZTwY3FvRWcleDfyYwcIMgz5dLSNLMZqwzQZK1DzvWeD6aGtBKCYPRftacHoESD+6bhukHZ6w95foRMJLOaBpkp+XfugFQioYvrM0AB1YQZ5DCQRhhc8jejwdY+bOB3eZ1lJY7Oannfu6XPW2fcknelyPt7PGf22rNfAgMBAAGjgYwwgYkwHQYDVR0OBBYEFK3Ah+Do3/zB9XjZ66i4biDpUEbAMGgGA1UdEQRhMF+CImlkc2VydmVyLnNlcnZpemljaWUuaW50ZXJuby5nb3YuaXSGOWh0dHBzOi8vaWRzZXJ2ZXIuc2Vydml6aWNpZS5pbnRlcm5vLmdvdi5pdC9pZHAvc2hpYmJvbGV0aDANBgkqhkiG9w0BAQsFAAOCAQEAVtpn/s+lYVf42pAtdgJnGTaSIy8KxHeZobKNYNFEY/XTaZEt9QeV5efUMBVVhxKTTHN0046DR96WFYXs4PJ9Fpyq6Hmy3k/oUdmHJ1c2bwWF/nZ82CwOO081Yg0GBcfPEmKLUGOBK8T55ncW+RSZadvWTyhTtQhLUtLKcWyzKB5aS3kEE5LSzR8sw3owln9P41Mz+QtL3WeNESRHW0qoQkFotYXXW6Rvh69+GyzJLxvq2qd7D1qoJgOMrarshBKKPk+ABaLYoEf/cru4e0RDIp2mD0jkGOGDkn9XUl+3ddALq/osTki6CEawkhiZEo6ABEAjEWNkH9W3/ZzvJnWo6Q==',
        ];

        return $metadata;
    }

    private function idpSpid($metadata)
    {
        $download_params = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => true,
            ]
        ];

        $xml = file_get_contents('https://registry.spid.gov.it/entities-idp', false, stream_context_create($download_params));

        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$3", $xml);
        $xml = simplexml_load_string($xml);
        $xmlDom = dom_import_simplexml($xml);

        $registry_idp_json = file_get_contents('https://registry.spid.gov.it/entities-idp?output=json');
        $registry_idp = json_decode($registry_idp_json, true);

        if (config('spid-cie-oidc.debug') || config('spid-cie-oidc.validator')) {
            $xml_demo = file_get_contents('https://demo.spid.gov.it/metadata.xml', false, stream_context_create($download_params));
            $xml_demo = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$3", $xml_demo);
            $xml_demo = simplexml_load_string($xml_demo);
            $xml_demo->Organization->OrganizationName = "SPID Demo";
            $xml_demo_dom = dom_import_simplexml($xml_demo);
            $xmlDom->appendChild($xmlDom->ownerDocument->importNode($xml_demo_dom, true));

            $xml_validator = file_get_contents('https://demo.spid.gov.it/validator/metadata.xml', false, stream_context_create($download_params));
            $xml_validator = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$3", $xml_validator);
            $xml_validator = simplexml_load_string($xml_validator);
            $xml_validator->Organization->OrganizationName = "SPID Validator";
            $xml_validator_dom = dom_import_simplexml($xml_validator);
            $xmlDom->appendChild($xmlDom->ownerDocument->importNode($xml_validator_dom, true));
        }

        foreach ($xml->EntityDescriptor as $entity) {
            $OrganizationName = trim($entity->Organization->OrganizationName);
            $OrganizationDisplayName = trim($entity->Organization->OrganizationDisplayName);
            $OrganizationURL = trim($entity->Organization->OrganizationURL);
            $IDPentityID = trim($entity->attributes()['entityID']);

            $template_keys = [];
            foreach ($entity->IDPSSODescriptor->KeyDescriptor as $keyDescriptor) {
                $X509Certificate = trim($keyDescriptor->KeyInfo->X509Data->X509Certificate);
                $template_keys[] = [
                    'encryption' => false,
                    'signing' => true,
                    'type' => 'X509Certificate',
                    'X509Certificate' => $X509Certificate,
                ];
            }

            $NameIDFormat = trim($entity->IDPSSODescriptor->NameIDFormat);

            $template_slo = [];
            foreach ($entity->IDPSSODescriptor->SingleLogoutService as $slo) {
                $SLOBinding = trim($slo->attributes()['Binding']);
                $SLOLocation = trim($slo->attributes()['Location']);
                $template_slo[] = [
                    'Binding' => $SLOBinding,
                    'Location' => $SLOLocation,
                ];
            }

            $template_sso = [];
            foreach ($entity->IDPSSODescriptor->SingleSignOnService as $sso) {
                $SSOBinding = trim($sso->attributes()['Binding']);
                $SSOLocation = trim($sso->attributes()['Location']);
                $template_sso[] = [
                    'Binding' => $SSOBinding,
                    'Location' => $SSOLocation,
                ];
            }

            $icon = "/assets/icons/spid-idp-dummy.png";

            foreach($registry_idp as $registry_idp_entity) {
                if($registry_idp_entity['entity_id'] == $IDPentityID) {
                    $icon = $registry_idp_entity['logo_uri'];
                }
            }

            $metadata[$IDPentityID] = [
                'entityid' => env('APP_URL'),
                'description' => [
                    'en' => $OrganizationName,
                    'it' => $OrganizationName,
                ],
                'icon' => $icon,
                'OrganizationName' => [
                    'en' => $OrganizationName,
                    'it' => $OrganizationName,
                ],
                'name' => [
                    'en' => $OrganizationName,
                    'it' => $OrganizationName,
                ],
                'OrganizationDisplayName' => [
                    'en' => $OrganizationDisplayName,
                    'it' => $OrganizationDisplayName,
                ],
                'url' => [
                    'en' => $OrganizationURL,
                    'it' => $OrganizationURL,
                ],
                'OrganizationURL' => [
                    'en' => $OrganizationURL,
                    'it' => $OrganizationURL,
                ],
                'contacts' => [],
                'metadata-set' => 'saml20-idp-remote',
                'sign.authnrequest' => true,
                'SingleSignOnService' => $template_sso,
                'SingleLogoutService' => $template_slo,
                'ArtifactResolutionService' => [],
                'NameIDFormats' => [
                    $NameIDFormat,
                ],
                'keys' => $template_keys,
            ];
        }

        return $metadata;
    }

    private function commonAuthSource()
    {
        $entity_id = config('spid-cie-oidc.saml.entityid', null);
        if (is_null($entity_id)) {
            $entity_id = env('APP_URL');
        }

        return [
            'saml:SP',
            'entityID' => $entity_id,

            'name' => [
                'it' => config('spid-cie-oidc.organization.name')
            ],
            'description' => [
                'it' => config('spid-cie-oidc.organization.name')
            ],
            'OrganizationName' => [
                'it' => config('spid-cie-oidc.organization.name')
            ],
            'OrganizationDisplayName' => [
                'it' => config('spid-cie-oidc.organization.name')
            ],
            'OrganizationURL' => [
                'it' => env('APP_URL')
            ],

            'idp' => null,
            'discoURL' => null,

            'acs.Bindings' => [
                'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST'
            ],

            'AuthnContextComparison' => 'exact',
            'ForceAuthn' => true,
            'sign.authnrequest' => true,
            'sign.logout' => true,
            'AuthnRequestsSigned' => true,
            'WantAssertionsSigned' => true,

            'attributes.index' => 0,
            'attributes.NameFormat' => 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
            'attributes' => config('spid-cie-oidc.client.user_attributes.saml'),

            'metadata.sign.enable' => true,
            'metadata.sign.algorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',
            'metadata.supported.protocols' => [
                'urn:oasis:names:tc:SAML:2.0:protocol'
            ],

            'signature.algorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',

            'NameIDPolicy' => [
                'Format' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
                'AllowCreate' => true,
            ],
        ];
    }

    private function samlAuthSourceCie()
    {
        $ret = $this->commonAuthSource();

        $ret['privatekey'] = config('spid-cie-oidc.certs.saml_cie_pem');
        $ret['certificate'] = config('spid-cie-oidc.certs.saml_cie_crt');

        $ret['contacts'] = [
            [
                'contactType' => 'administrative',
                'company' => config('spid-cie-oidc.organization.name'),
                'emailAddress' => config('spid-cie-oidc.organization.contacts.email'),
                'telephoneNumber' => config('spid-cie-oidc.organization.contacts.phone'),
                'extensions' => [
                    'ns' => 'cie:https://www.cartaidentita.interno.gov.it/saml-extensions',
                    'elements' => [
                        'cie:Public' => NULL,
                        'cie:IPACode' => config('spid-cie-oidc.organization.identifier'),
                        'cie:Municipality' => config('spid-cie-oidc.organization.istatLocality'),
                        'cie:Province' => config('spid-cie-oidc.organization.district'),
                        'cie:Country' => 'IT',
                    ],
                ],
            ],
        ];

        return $ret;
    }

    private function samlAuthSourceSpid()
    {
        $ret = $this->commonAuthSource();

        $ret['privatekey'] = config('spid-cie-oidc.certs.saml_spid_pem');
        $ret['certificate'] = config('spid-cie-oidc.certs.saml_spid_crt');
        $ret['AuthnContextClassRef'] = config('spid-cie-oidc.client.acr');
        $ret['AttributeConsumingServiceIndex'] = 0;

        $ret['contacts'] = [
            [
                'contactType' => 'other',
                'spid' => true,
                'spid.codeType' => 'IPACode',
                'spid.codeValue' => config('spid-cie-oidc.organization.code'),
                'company' => config('spid-cie-oidc.organization.name'),
                'emailAddress' => config('spid-cie-oidc.organization.contacts.email'),
                'telephoneNumber' => config('spid-cie-oidc.organization.contacts.phone'),
            ]
        ];

        return $ret;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $templates_folder = dirname(__FILE__) . '/templates/';

        $enabled = config('spid-cie-oidc.protocols');
        $do_cie = ($enabled['cie'] == 'saml');
        $do_spid = ($enabled['spid'] == 'saml');

        $saml_service_name = config('spid-cie-oidc.routes.saml.module');
        $plain_domain = parse_url(env('APP_URL'), PHP_URL_HOST);
        $plain_domain = preg_replace('/^www\./', '', $plain_domain);

        $config_folder = config('spid-cie-oidc.saml.simplesaml.folder', false);
        if ($config_folder) {
            $config_folder = rtrim($config_folder, '/');
            $config_path =  $config_folder . '/config.php';
            $authsources_path = $config_folder . '/authsources.php';
            $metadata_path = $config_folder . '/metadata/saml20-idp-remote.php';
            $metadata_in_config = $config_folder . '/metadata';

            if (file_exists($config_folder) == false) {
                mkdir($config_folder, 0766, true);
            }

            if (file_exists($metadata_in_config) == false) {
                mkdir($metadata_in_config, 0766, true);
            }
        }
        else {
            $config_path = base_path("vendor/simplesamlphp/simplesamlphp/config/config.php");
            $authsources_path = base_path("vendor/simplesamlphp/simplesamlphp/config/authsources.php");
            $metadata_path = base_path("vendor/simplesamlphp/simplesamlphp/metadata/saml20-idp-remote.php");
            $metadata_in_config = 'metadata';
        }

        /*
            Configurazione di base
        */

        $vars = [
            '{{BASEURLPATH}}' => "'" . $saml_service_name . "/'",
            '{{ADMIN_PASSWORD}}' => "'" . Str::random(20) . "'",
            '{{SECRETSALT}}' => "'" . bin2hex(random_bytes(16)) . "'",
            '{{TECHCONTACT_NAME}}' => "'" . config('spid-cie-oidc.technical.name') . "'",
            '{{TECHCONTACT_EMAIL}}' => "'" . config('spid-cie-oidc.technical.email') . "'",
            '{{SP_DOMAIN}}' => "'." . $plain_domain . "'",
            '{{METADATA_FOLDER}}' => "'" . $metadata_in_config . "'",
        ];

        $template = file_get_contents($templates_folder . 'simplesaml-config.tpl');
        $customized = str_replace(array_keys($vars), $vars, $template);
        file_put_contents($config_path, $customized);

        /*
            File delle auth sources
        */

        $vars = [
            '{{AUTHSOURCE_CIE}}' => $do_cie ? "'cie' => " . var_export($this->samlAuthSourceCie(), true) . ",\n" : '',
            '{{AUTHSOURCE_SPID}}' => $do_spid ? "'spid' => " . var_export($this->samlAuthSourceSpid(), true) . ",\n" : '',
        ];

        $template = file_get_contents($templates_folder . 'simplesaml-authsources.tpl');
        $customized = str_replace(array_keys($vars), $vars, $template);
        file_put_contents($authsources_path, $customized);

        /*
            Files degli IdP
        */

        $IDPMetadata = [];

        if ($do_cie) {
            $IDPMetadata = $this->idpCie($IDPMetadata);
        }

        if ($do_spid) {
            $IDPMetadata = $this->idpSpid($IDPMetadata);
        }

        $customized = "<?php\n\n";
        foreach($IDPMetadata as $id => $data) {
            $customized .= "\$metadata['" . $id . "'] = " . var_export($data, true) . ";\n\n";
        }
        file_put_contents($metadata_path, $customized);
    }
}
