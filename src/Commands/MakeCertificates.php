<?php

namespace MadBob\SCOL\Commands;

use Illuminate\Console\Command;

use SPID_CIE_OIDC_PHP\OIDC\RP\Database as RP_Database;

class MakeCertificates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scol:make-certificates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea i certificati';

    private function checkFolder($path)
    {
        if (file_exists($path) == false) {
            mkdir($path, 0755, true);
        }
    }

    private function initDB()
    {
        $dbpath = storage_path('oidc/data/db.sqlite3');
        $this->checkFolder(dirname($dbpath));
        $db = new RP_Database($dbpath);
    }

    private function commonIntro($openssl_config)
    {
        fwrite($openssl_config, "\n[ req ]\n");
        fwrite($openssl_config, "default_bits = 3072\n");
        fwrite($openssl_config, "default_md = sha256\n");
        fwrite($openssl_config, "distinguished_name = dn\n");
        fwrite($openssl_config, "encrypt_key = no\n");
        fwrite($openssl_config, "prompt = no\n");
        fwrite($openssl_config, "req_extensions  = req_ext\n");
    }

    private function commonDN($openssl_config)
    {
        fwrite($openssl_config, "\n[ dn ]\n");
        fwrite($openssl_config, "organizationName=" . config('spid-cie-oidc.organization.name') . "\n");
        fwrite($openssl_config, "commonName=" . config('spid-cie-oidc.organization.name') . "\n");
        fwrite($openssl_config, "uri=" . config('spid-cie-oidc.client.id') . "\n");
        fwrite($openssl_config, "organizationIdentifier=" . config('spid-cie-oidc.organization.identifier') . "\n");
        fwrite($openssl_config, "countryName=" . config('spid-cie-oidc.organization.country', 'IT') . "\n");
        fwrite($openssl_config, "localityName=" . config('spid-cie-oidc.organization.locality') . "\n");
    }

    private function executeShell($command)
    {
        $this->info($command);
        shell_exec($command);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path = storage_path('oidc/certs');
        $this->checkFolder($path);

        $cie = config('spid-cie-oidc.protocols.cie');
        $spid = config('spid-cie-oidc.protocols.spid');

        switch($cie) {
            case 'oidc':
                $openssl_config = fopen("$path/cie-openssl.cnf", "w");
                fwrite($openssl_config, "oid_section = spid_oids\n");

                $this->commonIntro($openssl_config);

                fwrite($openssl_config, "\n[ spid_oids ]\n");
                fwrite($openssl_config, "spid-privatesector-SP=1.3.76.16.4.3.1\n");
                fwrite($openssl_config, "spid-publicsector-SP=1.3.76.16.4.2.1\n");
                fwrite($openssl_config, "uri=2.5.4.83\n");

                $this->commonDN($openssl_config);

                fwrite($openssl_config, "\n[ req_ext ]\n");
                fwrite($openssl_config, "certificatePolicies = @spid_policies\n");

                fwrite($openssl_config, "\n[ spid_policies ]\n");
                switch (config('spid-cie-oidc.organization.is_pa', false)) {
                    case true:
                        fwrite($openssl_config, "policyIdentifier = spid-publicsector-SP\n");
                        break;
                    case false:
                        fwrite($openssl_config, "policyIdentifier = spid-privatesector-SP\n");
                        break;
                }

                fclose($openssl_config);

                $pem_path = config('spid-cie-oidc.certs.private_fed_sig');
                $this->checkFolder(dirname($pem_path));
                $crt_path = config('spid-cie-oidc.certs.public_fed_sig');
                $this->checkFolder(dirname($crt_path));

                $this->executeShell("openssl req -new -x509 -config '$path/cie-openssl.cnf' -days 730 -keyout '$pem_path' -out '$crt_path' -extensions req_ext");

                $pem_path = config('spid-cie-oidc.certs.private_core_sig');
                $this->checkFolder(dirname($pem_path));
                $crt_path = config('spid-cie-oidc.certs.public_core_sig');
                $this->checkFolder(dirname($crt_path));

                $this->executeShell("openssl req -new -x509 -config '$path/cie-openssl.cnf' -days 730 -keyout '$pem_path' -out '$crt_path' -extensions req_ext");

                $pem_path = config('spid-cie-oidc.certs.private_core_enc');
                $this->checkFolder(dirname($pem_path));
                $crt_path = config('spid-cie-oidc.certs.public_core_enc');
                $this->checkFolder(dirname($crt_path));

                $this->executeShell("openssl req -new -x509 -config '$path/cie-openssl.cnf' -days 730 -keyout '$pem_path' -out '$crt_path' -extensions req_ext");

                break;

            case 'saml':
                $openssl_config = fopen("$path/cie-openssl.cnf", "w");
                fwrite($openssl_config, "oid_section = cie_oids\n");

                $this->commonIntro($openssl_config);

                fwrite($openssl_config, "\n[ cie_oids ]\n");
                fwrite($openssl_config, "uri=2.5.4.83\n");

                $this->commonDN($openssl_config);

                fwrite($openssl_config, "\n[ req_ext ]\n");

                fclose($openssl_config);

                $pem_path = config('spid-cie-oidc.certs.saml_cie_pem');
                $this->checkFolder(dirname($pem_path));
                $crt_path = config('spid-cie-oidc.certs.saml_cie_crt');
                $this->checkFolder(dirname($crt_path));

                $this->executeShell("openssl req -new -x509 -config '$path/cie-openssl.cnf' -days 730 -keyout '$pem_path' -out '$crt_path' -extensions req_ext");

                break;
        }

        switch($spid) {
            case 'oidc':
                $this->error('Protocollo OIDC per SPID non ancora supportato');
                exit(1);
                break;

            case 'saml':
                $is_pa = config('spid-cie-oidc.organization.is_pa', true);

                $openssl_config = fopen("$path/spid-openssl.cnf", "w");
                fwrite($openssl_config, "oid_section = spid_oids\n");

                $this->commonIntro($openssl_config);

                fwrite($openssl_config, "\n[ spid_oids ]\n");
                fwrite($openssl_config, "agidcert=1.3.76.16.6\n");

                if ($is_pa) {
                    fwrite($openssl_config, "spid-publicsector-SP=1.3.76.16.4.2.1\n");
                }
                else {
                    fwrite($openssl_config, "spid-privatesector-SP=1.3.76.16.4.3.1\n");
                }

                fwrite($openssl_config, "uri=2.5.4.83\n");

                $this->commonDN($openssl_config);

                fwrite($openssl_config, "\n[ req_ext ]\n");
                fwrite($openssl_config, "basicConstraints=CA:FALSE\n");
                fwrite($openssl_config, "keyUsage=critical,digitalSignature,nonRepudiation\n");
                fwrite($openssl_config, "certificatePolicies=@agid_policies,@spid_policies\n");

                fwrite($openssl_config, "\n[ agid_policies ]\n");
                fwrite($openssl_config, "policyIdentifier=agidcert\n");
                fwrite($openssl_config, "userNotice=@agidcert_notice\n");

                fwrite($openssl_config, "\n[ agidcert_notice ]\n");
                fwrite($openssl_config, "explicitText=\"agIDcert\"\n");

                fwrite($openssl_config, "\n[ spid_policies ]\n");

                if ($is_pa) {
                    fwrite($openssl_config, "policyIdentifier = spid-publicsector-SP\n");
                }
                else {
                    fwrite($openssl_config, "policyIdentifier = spid-privatesector-SP\n");
                }

                fwrite($openssl_config, "userNotice=@spid_notice\n");
                fwrite($openssl_config, "\n[ spid_notice ]\n");

                if ($is_pa) {
                    fwrite($openssl_config, "explicitText=\"cert_SP_Pub\"\n");
                }
                else {
                    fwrite($openssl_config, "explicitText=\"cert_SP_Priv\"\n");
                }

                fclose($openssl_config);

                $pem_path = config('spid-cie-oidc.certs.saml_spid_pem');
                $this->checkFolder(dirname($pem_path));
                $crt_path = config('spid-cie-oidc.certs.saml_spid_crt');
                $this->checkFolder(dirname($crt_path));

                $this->executeShell("openssl req -new -x509 -config '$path/spid-openssl.cnf' -days 730 -keyout '$pem_path' -out '$crt_path' -extensions req_ext");

                break;
        }

        $this->initDB();
    }
}
