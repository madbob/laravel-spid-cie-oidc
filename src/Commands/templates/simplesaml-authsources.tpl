<?php

use SimpleSAML\Configuration;
use SimpleSAML\Database;

define('AUTHSOURCES_DATABASE_TABLE', 'authsources');

$config = array(
    'admin' => array(
        'core:AdminPassword',
    ),

    {{AUTHSOURCE_SPID}}

    {{AUTHSOURCE_CIE}}
);
