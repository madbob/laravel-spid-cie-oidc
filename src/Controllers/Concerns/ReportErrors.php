<?php

namespace MadBob\SCOL\Controllers\Concerns;

use Illuminate\Foundation\Exceptions\RegisterErrorViewPaths;
use Illuminate\Support\Facades\View;

trait ReportErrors
{
    protected function displayError($code, $description)
    {
        (new RegisterErrorViewPaths)();
        $custom_view = config('spid-cie-oidc.views.errors');
        if (is_null($custom_view) || View::exists($custom_view) == false) {
            $custom_view = 'scol::error';
        }

        return view($custom_view, [
            'code' => $code,
            'error_description' => $description,
        ]);
    }
}
