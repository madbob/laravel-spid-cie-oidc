<?php

namespace MadBob\SCOL\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use SPID_CIE_OIDC_PHP\Core\Util;
use SPID_CIE_OIDC_PHP\Federation\EntityStatement;
use SPID_CIE_OIDC_PHP\Federation\ResolveEndpoint;
use SPID_CIE_OIDC_PHP\Federation\TrustChain;
use SPID_CIE_OIDC_PHP\OIDC\RP\AuthenticationRequest;
use SPID_CIE_OIDC_PHP\OIDC\RP\Database as RP_Database;
use SPID_CIE_OIDC_PHP\OIDC\RP\TokenRequest;
use SPID_CIE_OIDC_PHP\OIDC\RP\UserinfoRequest;
use SPID_CIE_OIDC_PHP\OIDC\RP\RevocationRequest;

use MadBob\SCOL\Controllers\Concerns\ReportErrors;

class OIDCController extends Controller
{
    use ReportErrors;

    private function rpDatabase()
    {
        $dbpath = storage_path('oidc/data/db.sqlite3');
        return new RP_Database($dbpath);
    }

    /*
        Questa funzione traduce la configurazione del modulo ed eventuali valori
        di default in un array piatto, usato dalle classi esistenti di
        SPID_CIE_OIDC_PHP (con tutte le sue idiosincrasie...)
    */
    private function resolveConfig()
    {
        return [
            'cert_public' => config('spid-cie-oidc.certs.public_core_sig'),
            'cert_public_core_sig' => config('spid-cie-oidc.certs.public_core_sig'),
            'cert_public_core_enc' => config('spid-cie-oidc.certs.public_core_enc'),
            'cert_private_core_sig' => config('spid-cie-oidc.certs.private_core_sig'),
            'cert_enc_public' => config('spid-cie-oidc.certs.public_core_enc'),
            'cert_enc_private' => config('spid-cie-oidc.certs.private_core_enc'),
            'cert_public_fed' => config('spid-cie-oidc.certs.public_fed_sig'),
            'cert_private_fed' => config('spid-cie-oidc.certs.private_fed_sig'),

            'homepage_uri' => config('spid-cie-oidc.organization.home'),
            'logo_uri' => config('spid-cie-oidc.organization.logo'),
            'organization_name' => config('spid-cie-oidc.organization.name'),
            'contacts' => [config('spid-cie-oidc.organization.contacts.email')],
            'policy_uri' => config('spid-cie-oidc.organization.policy_url'),

            'client_id' => config('spid-cie-oidc.client.id'),
            'client_registration_types' => ['automatic'],
            'application_type' => 'web',
            'redirect_uri' => route('scol.redirect'),
            'client_name' => config('spid-cie-oidc.client.name'),
            'subject_type' => 'pairwise',
            'authority_hint' => config('spid-cie-oidc.client.authority_hint'),
            'trust_marks' => config('spid-cie-oidc.client.trust_marks'),
        ];
    }

    public function wellknownFederation(Request $request)
    {
        $output = $request->input('output', 'default');
        $json = ($output == 'json');

        $config = $this->resolveConfig();
        $data = EntityStatement::makeFromConfig($config, $json);

        $mediaType = $json ? 'application/json' : 'application/entity-statement+jwt';
        return response($data)->header('Content-Type', $mediaType);
    }

    public function initAuth(Request $request, $ta, $op)
    {
        $ta_id = base64_decode($ta);
        $op_id = base64_decode($op);

        $auth = Session::get('oidc_auth');
        $state = $auth['state'] ?? null;
        if ($state == null) {
            $state = $request->input('state');
            if ($state == null) {
                $state = 'state';
            }
        }

        $rp_database = $this->rpDatabase();
        $config = $this->resolveConfig();

        $acr = config('spid-cie-oidc.client.acr');
        $user_attributes = config('spid-cie-oidc.client.user_attributes.oidc');
        $redirect_uri = route('scol.redirect');
        $req_id = $rp_database->createRequest($ta_id, $op_id, $redirect_uri, $state, $acr, $user_attributes);
        $request = $rp_database->getRequest($req_id);
        $code_verifier = $request['code_verifier'];
        $nonce = $request['nonce'];

        try {
            $trustchain = new TrustChain($config, $rp_database, $op_id, $ta_id);
            $configuration = $trustchain->resolve();
        }
        catch (\Exception $e) {
            \Log::debug('Fallita inizializzazione autenticazione: ' . $e->getMessage());
            abort(401);
        }

        $authorization_endpoint = $configuration->metadata->openid_provider->authorization_endpoint;
        $op_issuer = $configuration->metadata->openid_provider->issuer;

        $config = $this->resolveConfig();
        $authenticationRequest = new AuthenticationRequest($config);

        $redirect = $authenticationRequest->getRedirectURL(
            $op_issuer,
            $authorization_endpoint,
            $acr,
            $user_attributes,
            $code_verifier,
            $nonce,
            Util::base64UrlEncode(str_pad($req_id, 32))
        );

        return redirect()->away($redirect);
    }

    public function handleAuth(Request $request)
    {
        $rp_database = $this->rpDatabase();
        $config = $this->resolveConfig();

        $error = $request->input('error');
        if ($error != null) {
            return $this->displayError($error, $request->input('error_description'));
        }

        $code = $request->input('code');
        $req_id = trim(Util::base64UrlDecode($request->input('state')));
        $iss = $request->input('iss');

        $dbrequest = $rp_database->getRequest($req_id);
        $ta_id = $dbrequest['ta_id'];
        $op_id = $dbrequest['op_id'];
        $state = $dbrequest['state'];
        $code_verifier = $dbrequest['code_verifier'];

        try {
            $trustchain = new TrustChain($config, $rp_database, $op_id, $ta_id);
            $configuration = $trustchain->resolve();
        }
        catch (\Exception $e) {
            \Log::debug('Fallita verifica autenticazione: ' . $e->getMessage());
            abort(401);
        }

        $token_endpoint = $configuration->metadata->openid_provider->token_endpoint;
        $userinfo_endpoint = $configuration->metadata->openid_provider->userinfo_endpoint;

        try {
            $tokenRequest = new TokenRequest($config);
            $tokenResponse = $tokenRequest->send($token_endpoint, $code, $code_verifier);

            $access_token = $tokenResponse->access_token;

            $userinfoRequest = new UserinfoRequest($config, $configuration->metadata->openid_provider);
            $userinfoResponse = $userinfoRequest->send($userinfo_endpoint, $access_token);

            /*
                Qui conservo in sessione alcuni parametri utili poi in fase di
                logout o in altre sedi
            */
            Session::put('oidc_auth', [
                'ta_id' => $ta_id,
                'op_id' => $op_id,
                'access_token' => $access_token,
                'state' => $state,
            ]);

            $user_operation = config('spid-cie-oidc.operations.login');
            $op = new $user_operation();

            return $op->handleLogin($userinfoResponse, [
                'ta_id' => $ta_id,
                'op_id' => $op_id,
                'access_token' => $access_token,
                'state' => $state,
            ]);
        }
        catch (\Exception $e) {
            $code = in_array($e->getCode(), [200, 301, 302, 400, 401, 404])? $e->getCode() : 500;
            abort($code);
        }
    }

    public function resolve(Request $request)
    {
        $sub = $request->input('sub');
        $anchor = $request->input('anchor');
        if (empty($sub) || empty($anchor)) {
            abort(400);
        }

        $rp_database = $this->rpDatabase();
        $config = $this->resolveConfig();

        $data = ResolveEndpoint::resolve($config, $rp_database, $sub, $anchor);
        return response($data)->header('Content-Type', 'application/entity-statement+jwt');
    }

    public function logout()
    {
        $rp_database = $this->rpDatabase();
        $config = $this->resolveConfig();

        $auth = Session::get('oidc_auth');
        $ta_id = $auth['ta_id'] ?? null;
        $op_id = $auth['op_id'] ?? null;
        $access_token = $auth['access_token'] ?? null;

        if ($access_token == null) {
            return redirect(config('spid-cie-oidc.routes.logout'));
        }

        try {
            $trustchain = new TrustChain($config, $rp_database, $op_id, $ta_id);
            $configuration = $trustchain->resolve();
        }
        catch (\Exception $e) {
            \Log::debug('Fallito logout: ' . $e->getMessage());
            abort(401);
        }

        $revocation_endpoint = $configuration->metadata->openid_provider->revocation_endpoint;

        try {
            $revocationRequest = new RevocationRequest($config);
            $revocationResponse = $revocationRequest->send($revocation_endpoint, $access_token);
        }
        catch (\Exception $e) {
            // do not null
        }
        finally {
            Session::foget('oidc_auth');
        }

        $user_operation = config('spid-cie-oidc.operations.logout');
        $op = new $user_operation();
        return $op->handleLogout();
    }
}
