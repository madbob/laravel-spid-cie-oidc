<?php

namespace MadBob\SCOL\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use MadBob\SCOL\Helpers\IdpList;
use MadBob\SCOL\Controllers\Concerns\ReportErrors;

class SAMLController extends Controller
{
    use ReportErrors;

    private function getSAML($idp)
    {
        $saml_config_folder = config('spid-cie-oidc.saml.simplesaml.folder');
        if ($saml_config_folder) {
            putenv('SIMPLESAMLPHP_CONFIG_DIR=' . $saml_config_folder);
        }

        if (str_starts_with($idp, 'CIE')) {
            $service = 'cie';
        }
        else {
            $service = 'spid';
        }

        return new \SimpleSAML\Auth\Simple($service);
    }

    public function initAuth(Request $request)
    {
        $idp = $request->input('idp');
        $spid_auth = $this->getSAML($idp);
        $idps = IdpList::getList();

        $level = $request->input('level');
        if ($level) {
            $level = "https://www.spid.gov.it/SpidL" . $level;
        }
        else {
            $level = config('spid-cie-oidc.client.acr')[0];
        }

        if (str_starts_with($idp, 'CIE')) {
            $binding = \SAML2\Constants::BINDING_HTTP_POST;
        }
        else {
            $binding = \SAML2\Constants::BINDING_HTTP_REDIRECT;
        }

        $config = [
            'saml:AuthnContextClassRef' => $level,
            'saml:AuthnContextComparison' => \SAML2\Constants::COMPARISON_MINIMUM,
            'saml:idp' => $idps[$idp],
            'saml:NameIDPolicy' => [
                'Format' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient'
            ],
            'saml:AttributeConsumingServiceIndex' => null,
            'saml:SingleSignOnServiceProtocolBinding' => $binding,
            'ReturnTo' => route('scol.saml.redirect', ['idp' => $idp]),
        ];

        $spid_auth->login($config);
    }

    public function handleAuth(Request $request)
    {
        $idp = $request->input('idp');
        $spid_auth = $this->getSAML($idp);
        $idps = IdpList::getList();

        if ($spid_auth->isAuthenticated() && ($idps[$idp] == $spid_auth->getAuthData('saml:sp:IdP'))) {
            $userinfoResponse = $spid_auth->getAttributes();

            $flat_userinfoResponse = (object) [];
            foreach($userinfoResponse as $key => $value) {
                if (is_array($value)) {
                    $value = $value[0];
                }

                $flat_userinfoResponse->$key = $value;
            }

            $user_operation = config('spid-cie-oidc.operations.login');
            $op = new $user_operation();
            return $op->handleLogin($flat_userinfoResponse, []);
        }
    }

    public function reportError($code)
    {
        switch ($code) {
            case 19:
                $message = 'Autenticazione fallita per ripetuta sottomissione di credenziali errate (superato numero tentativi secondo le policy adottate)';
                break;
            case 20:
                $message = 'Utente privo di credenziali compatibili con il livello richiesto dal fornitore del servizio';
                break;
            case 21:
                $message = 'Timeout durante l’autenticazione utente';
                break;
            case 22:
                $message = 'Utente nega il consenso all’invio di dati al SP in caso di sessione vigente';
                break;
            case 23:
                $message = 'Utente con identità sospesa/revocata o con credenziali bloccate';
                break;
            case 25:
                $message = 'Processo di autenticazione annullato dall’utente';
                break;
            case 25:
                $message = 'Processo di autenticazione annullato dall’utente';
                break;
            case 30:
                $message = 'Il SP ha richiesto di autenticare l’utente con una identità digitale diversa da quella utilizzata dall’utente';
                break;
            default:
                $message = 'Errore non identificato';
                break;
        }

        return $this->displayError($code, $message);
    }
}
