<?php

namespace MadBob\SCOL\Events;

use Illuminate\Foundation\Events\Dispatchable;

class UserAuthenticated
{
    use Dispatchable;

    public $info;

    public function __construct($info)
    {
        $this->info = $info;
    }
}
