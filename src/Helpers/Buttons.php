<?php

namespace MadBob\SCOL\Helpers;

use MadBob\SCOL\Helpers\IdpList;

class Buttons
{
    public function buttonSpid()
    {
        $protocol = config('spid-cie-oidc.protocols.spid');

        switch($protocol) {
            case 'oidc':
                return null;

            case 'saml':
                return IdpList::getListSpid();
        }

        return null;
    }

    public function buttonCie()
    {
        $protocol = config('spid-cie-oidc.protocols.cie');

        switch($protocol) {
            case 'oidc':
                $authority = config('spid-cie-oidc.client.authority_hint');
                $provider = config('spid-cie-oidc.client.cie.provider');
                return route('scol.oidc.init', ['ta' => base64_encode($authority), 'op' => base64_encode($provider)]);

            case 'saml':
                return route('scol.saml.init', ['idp' => 'CIE']);
        }

        return null;
    }
}
