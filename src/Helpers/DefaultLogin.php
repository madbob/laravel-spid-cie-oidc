<?php

/*
    Classe di esempio per la gestione del login.
    Si raccomanda di provvedere alla propria implementazione, da impostare poi
    nella propria configurazione di spid-cie-oidc.operations.login
*/

namespace MadBob\SCOL\Helpers;

use Illuminate\Support\Facades\Auth;

use App\Models\User;
use MadBob\SCOL\Interfaces\UserLogin;

class DefaultLogin implements UserLogin
{
    /*
        $userinfo è un oggetto con i parametri richiesti per l'utente
        autenticato (gli attributi enumerati nella configurazione
        spid-cie-oidc.client.user_attributes). Attenzione: SAML e OIDC generano
        attributi con nomi diversi! Testare tutti i valori per gli attributi che
        si intendono verificare in questa fase!
        In caso di autenticazione OIDC, $details è un array associativo con
        alcuni attributi addizionali tra cui l'access token.
    */
    public function handleLogin($userinfo, $details)
    {
        $attr_oidc = 'https://attributes.eid.gov.it/fiscal_number';
        $attr_saml = 'fiscalNumber';

        if (isset($userinfo->$attr_oidc)) {
            $attr_name = $attr_oidc;
        }
        else {
            $attr_name = $attr_saml;
        }

        $user = User::where('fiscal_number', $userinfo->$attr_name)->first();
        if ($user) {
            Auth::loginUsingId($user->id);
        }

        /*
            La funzione deve ritornare essenzialmente la stessa cosa che
            ritornerebbe un Controller Laravel, auspicabilmente un redirect
            verso la homepage dell'utente autenticato
        */
        return redirect('/');
    }
}
