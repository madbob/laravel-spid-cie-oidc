<?php

/*
    Classe di esempio per la gestione del logout.
    Si raccomanda di provvedere alla propria implementazione, da impostare poi
    nella propria configurazione di spid-cie-oidc.operations.logout
*/

namespace MadBob\SCOL\Helpers;

use Illuminate\Support\Facades\Auth;

use MadBob\SCOL\Interfaces\UserLogout;

class DefaultLogout implements UserLogout
{
    public function handleLogout()
    {
        Auth::logout();

        /*
            La funzione deve ritornare essenzialmente la stessa cosa che
            ritornerebbe un Controller Laravel, auspicabilmente un redirect
            verso il pannello di login o, come in questo caso, verso la homepage
        */
        return redirect('/');
    }
}
