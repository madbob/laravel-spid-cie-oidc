<?php

namespace MadBob\SCOL\Helpers;

use Illuminate\Support\Facades\Cache;

class IdpList
{
    /*
        Questo legge dinamicamente l'elenco dei provider SPID e restituisce un
        array strutturato, che può essere usato per generare i relativi links
        per l'autenticazione
    */
    public static function getListSpid()
    {
        $ret = [];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, 'https://registry.spid.gov.it/entities-idp?output=json');

        /*
            Non è raro che registry.spid.gov.it abbia un certificato HTTPS non
            valido, qui saltiamo la verifica
        */
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $registry_idp_json = curl_exec($ch);
        curl_close($ch);

        /*
            Non è raro che registry.spid.gov.it sia scassato e non ritorni
            affatto il JSON con l'elenco degli IdP. Pertanto quando posso metto
            i risultati nella cache locale di Laravel, da cui attingo in caso di
            problemi
        */

        $fetched = false;
        
        if ($registry_idp_json) {
            $registry_idp = json_decode($registry_idp_json, true);

            if (is_array($registry_idp)) {
                foreach($registry_idp as $registry_idp_entity) {
                    $ret[] = (object) [
                        'name' => $registry_idp_entity['organization_name'],
                        'icon' => $registry_idp_entity['logo_uri'],
                        'entity' => $registry_idp_entity['entity_id'],
                        'link' => route('scol.saml.init', ['idp' => $registry_idp_entity['organization_name']]),
                    ];
                }

                Cache::forever('laravel_spid_idp_list', $ret);
                $fetched = true;
            }
        }

        if ($fetched === false) {
            $ret = Cache::get('laravel_spid_idp_list');
        }

        if (config('spid-cie-oidc.debug') || config('spid-cie-oidc.validator')) {
            /*
                Nomi e URL delle istanze di debug devono corrispondere a quelli
                salvati nella configurazione SimpleSAML, generata nella funzione
                DeploySaml::idpSpid()
            */

            $debug = [
                'SPID Demo' => 'https://demo.spid.gov.it',
                'SPID Validator' => 'https://demo.spid.gov.it/validator',
            ];

            foreach($debug as $key => $value) {
                $ret[] = (object) [
                    'name' => $key,
                    'icon' => null,
                    'entity' => $value,
                    'link' => route('scol.saml.init', ['idp' => $key]),
                ];
            }
        }

        if (empty($ret)) {
            return null;
        }
        else {
            return $ret;
        }
    }

    /*
        Questo genera un semplice array associativo dove la chiave è il nome del
        provider e il valore è l'URL di riferimento
    */
    public static function getList()
    {
        $ret = [];

        $spid = self::getListSpid();
        if ($spid) {
            foreach($spid as $s) {
                $ret[$s->name] = $s->entity;
            }
        }

        if (config('spid-cie-oidc.debug')) {
            $ret['CIE'] = 'https://preproduzione.idserver.servizicie.interno.gov.it/idp/profile/SAML2/POST/SSO';
        }
        else {
            $ret['CIE'] = 'https://idserver.servizicie.interno.gov.it/idp/profile/SAML2/POST/SSO';
        }

        return $ret;
    }
}
