<?php

namespace MadBob\SCOL\Interfaces;

interface UserLogin
{
    public function handleLogin($userinfo, $details);
}
