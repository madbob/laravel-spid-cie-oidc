<?php

namespace MadBob\SCOL\Interfaces;

interface UserLogout
{
    public function handleLogout();
}
