<?php

namespace MadBob\SCOL;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

use MadBob\SCOL\Commands\DeploySaml;
use MadBob\SCOL\Commands\MakeCertificates;

class SCOLServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/spid-cie-oidc.php', 'spid-cie-oidc');
    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/spid-cie-oidc.php' => config_path('spid-cie-oidc.php')
        ], 'config');

        $this->loadViewsFrom(__DIR__ . '/../views', 'scol');
        $this->loadRoutesFrom(__DIR__ . '/routes.php');

        if ($this->app->runningInConsole()) {
            $this->commands([
                MakeCertificates::class,
                DeploySaml::class,
            ]);
        }
    }
}
