<?php

use MadBob\SCOL\Controllers\OIDCController;
use MadBob\SCOL\Controllers\SAMLController;

/*
    Queste rotte concernono l'implementazione OIDC, ma non devono avere un
    prefisso nell'URL
*/
Route::get('/.well-known/openid-federation', [OIDCController::class, 'wellknownFederation'])->name('scol.wellknown.federation');
Route::get('/resolve', [OIDCController::class, 'resolve'])->name('scol.resolve');

$prefix = config('spid-cie-oidc.routes.oidc.prefix');
Route::prefix($prefix)->group(function() {
    Route::get('/authz/{ta}/{op}', [OIDCController::class, 'initAuth'])->name('scol.oidc.init');

    $middleware = config('spid-cie-oidc.routes.middleware');
    Route::middleware($middleware)->group(function() {
        Route::get('/redirect', [OIDCController::class, 'handleAuth'])->name('scol.redirect');
        Route::get('/logout', [OIDCController::class, 'logout'])->name('scol.logout');
    });
});

$prefix = config('spid-cie-oidc.routes.saml.prefix');
Route::prefix($prefix)->group(function() {
    Route::get('/login', [SAMLController::class, 'initAuth'])->name('scol.saml.init');

    $middleware = config('spid-cie-oidc.routes.middleware');
    Route::middleware($middleware)->group(function() {
        Route::get('/redirect', [SAMLController::class, 'handleAuth'])->name('scol.saml.redirect');
        Route::get('/logout', [SAMLController::class, 'logout'])->name('scol.saml.logout');
    });
});

/*
    Questo endpoint non deve avere un prefisso arbitrario, in quanto l'URL è
    utilizzato dalla classe (eseguita nel contesto SimpleSAML, non Laravel)
    deps/saml/simplesamlphp/simplesamlphp/lib/SimpleSAML/Error/Error.php
*/
Route::get('/saml/error/{code}', [SAMLController::class, 'reportError'])->name('scol.saml.error');
