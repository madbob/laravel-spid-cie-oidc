@extends('errors::minimal')

@section('title', $error_description)
@section('code', $code)
@section('message', $error_description)
